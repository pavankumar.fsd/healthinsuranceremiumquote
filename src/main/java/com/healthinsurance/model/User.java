package com.healthinsurance.model;

// TODO: Auto-generated Javadoc
/**
 * The Class User.
 */
public class User {

	/** The name. */
	private String name;
	
	/** The gender. */
	private String gender;
	
	/** The age. */
	private int age;
	
	/** The current health. */
	private CurrentHealth currentHealth;
	
	/** The habits. */
	private Habits habits;

	/** The premium amount. */
	private int premiumAmount;

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the gender.
	 *
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * Sets the gender.
	 *
	 * @param gender the new gender
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * Gets the age.
	 *
	 * @return the age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * Sets the age.
	 *
	 * @param age the new age
	 */
	public void setAge(int age) {
		this.age = age;
	}

	/**
	 * Gets the premium amount.
	 *
	 * @return the premium amount
	 */
	public int getPremiumAmount() {
		return premiumAmount;
	}

	/**
	 * Sets the premium amount.
	 *
	 * @param premiumAmount the new premium amount
	 */
	public void setPremiumAmount(int premiumAmount) {
		this.premiumAmount = premiumAmount;
	}

	/**
	 * Gets the current health.
	 *
	 * @return the current health
	 */
	public CurrentHealth getCurrentHealth() {
		return currentHealth;
	}

	/**
	 * Sets the current health.
	 *
	 * @param currentHealth the new current health
	 */
	public void setCurrentHealth(CurrentHealth currentHealth) {
		this.currentHealth = currentHealth;
	}

	/**
	 * Gets the habits.
	 *
	 * @return the habits
	 */
	public Habits getHabits() {
		return habits;
	}

	/**
	 * Sets the habits.
	 *
	 * @param habits the new habits
	 */
	public void setHabits(Habits habits) {
		this.habits = habits;
	}

}
