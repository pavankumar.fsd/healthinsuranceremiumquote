package com.healthinsurance.model;

// TODO: Auto-generated Javadoc
/**
 * The Class Habits.
 */
public class Habits {

	/** The smoking. */
	private String smoking;
	
	/** The alcohol. */
	private String alcohol;
	
	/** The daily exercise. */
	private String dailyExercise;
	
	/** The drugs. */
	private String drugs;

	/**
	 * Gets the smoking.
	 *
	 * @return the smoking
	 */
	public String getSmoking() {
		return smoking;
	}

	/**
	 * Sets the smoking.
	 *
	 * @param smoking the new smoking
	 */
	public void setSmoking(String smoking) {
		this.smoking = smoking;
	}

	/**
	 * Gets the alcohol.
	 *
	 * @return the alcohol
	 */
	public String getAlcohol() {
		return alcohol;
	}

	/**
	 * Sets the alcohol.
	 *
	 * @param alcohol the new alcohol
	 */
	public void setAlcohol(String alcohol) {
		this.alcohol = alcohol;
	}

	/**
	 * Gets the daily exercise.
	 *
	 * @return the daily exercise
	 */
	public String getDailyExercise() {
		return dailyExercise;
	}

	/**
	 * Sets the daily exercise.
	 *
	 * @param dailyExercise the new daily exercise
	 */
	public void setDailyExercise(String dailyExercise) {
		this.dailyExercise = dailyExercise;
	}

	/**
	 * Gets the drugs.
	 *
	 * @return the drugs
	 */
	public String getDrugs() {
		return drugs;
	}

	/**
	 * Sets the drugs.
	 *
	 * @param drugs the new drugs
	 */
	public void setDrugs(String drugs) {
		this.drugs = drugs;
	}

}
