package com.healthinsurance.model;

// TODO: Auto-generated Javadoc
/**
 * The Class CurrentHealth.
 */
public class CurrentHealth {

	/** The hypertension. */
	private String hypertension;
	
	/** The blood pressure. */
	private String bloodPressure;
	
	/** The blood sugar. */
	private String bloodSugar;
	
	/** The overweight. */
	private String overweight;

	/**
	 * Gets the hypertension.
	 *
	 * @return the hypertension
	 */
	public String getHypertension() {
		return hypertension;
	}

	/**
	 * Sets the hypertension.
	 *
	 * @param hypertension the new hypertension
	 */
	public void setHypertension(String hypertension) {
		this.hypertension = hypertension;
	}

	/**
	 * Gets the blood pressure.
	 *
	 * @return the blood pressure
	 */
	public String getBloodPressure() {
		return bloodPressure;
	}

	/**
	 * Sets the blood pressure.
	 *
	 * @param bloodPressure the new blood pressure
	 */
	public void setBloodPressure(String bloodPressure) {
		this.bloodPressure = bloodPressure;
	}

	/**
	 * Gets the blood sugar.
	 *
	 * @return the blood sugar
	 */
	public String getBloodSugar() {
		return bloodSugar;
	}

	/**
	 * Sets the blood sugar.
	 *
	 * @param bloodSugar the new blood sugar
	 */
	public void setBloodSugar(String bloodSugar) {
		this.bloodSugar = bloodSugar;
	}

	/**
	 * Gets the overweight.
	 *
	 * @return the overweight
	 */
	public String getOverweight() {
		return overweight;
	}

	/**
	 * Sets the overweight.
	 *
	 * @param overweight the new overweight
	 */
	public void setOverweight(String overweight) {
		this.overweight = overweight;
	}

}
