package com.healthinsurance.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.healthinsurance.model.User;
import com.healthinsurance.util.Constants;
import com.healthinsurance.util.PercentageUtility;
import com.healthinsurance.util.Utils;

/**
 * The Class HealthInsQuoteController.
 */
@Controller
@RequestMapping(value = "/register")
public class HealthInsQuoteController {

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(HealthInsQuoteController.class);

	/**
	 * View registration.
	 *
	 * @param model
	 *            the model
	 * @return the string
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String viewRegistration(Map<String, Object> model) {
		final User userForm = new User();
		model.put("userForm", userForm);

		final List<String> yesnoList = new ArrayList<>();
		yesnoList.add(Constants.YES);
		yesnoList.add(Constants.NO);
		model.put("yesnoList", yesnoList);

		return Constants.REGISTRATION_PAGE;
	}

	/**
	 * Process registration.
	 *
	 * @param user
	 *            the user
	 * @param model
	 *            the model
	 * @return the string
	 */
	@RequestMapping(method = RequestMethod.POST)
	public String processRegistration(@ModelAttribute("userForm") User user, Map<String, Object> model) {

		int totalPremium = 5000;

		try {
			LOG.info("Health Insurance Premium Quota calculation started ...");

			if (user.getAge() < 18) {
				user.setPremiumAmount(totalPremium);
			} else {
				Utils.getCurrentHealth(user.getCurrentHealth());
				Utils.getHabits(user.getHabits());
				totalPremium = PercentageUtility.getInstance().getPremiumPercentageByAge(user.getAge(), totalPremium);
				totalPremium = PercentageUtility.getInstance().getPremiumPercentageByGender(user.getGender(),
						totalPremium);
				totalPremium = PercentageUtility.getInstance()
						.getPremiumPercentageByCurrentHealth(user.getCurrentHealth(), totalPremium);
				totalPremium = PercentageUtility.getInstance().getPremiumPercentageByHabits(user.getHabits(),
						totalPremium);
				user.setPremiumAmount(totalPremium);
			}
		} catch (Exception ex) {
			LOG.error("Exception Occured due to the : " + ex.getMessage());
		}
		LOG.info("Health Insurance Premium Quota calculation completed ...");
		return Constants.REGISTRATION_SUCCESS_PAGE;
	}

}
