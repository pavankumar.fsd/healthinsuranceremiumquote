package com.healthinsurance.util;

// TODO: Auto-generated Javadoc
/**
 * The Interface Constants.
 */
public interface Constants {

	/** The yes. */
	String YES = "Yes";
	
	/** The no. */
	String NO = "No";
	
	/** The registration page. */
	String REGISTRATION_PAGE= "Registration";
	
	/** The registration success page. */
	String REGISTRATION_SUCCESS_PAGE= "RegistrationSuccess";

}
